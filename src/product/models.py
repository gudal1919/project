from django.db import models

"""
{
        "battery": "3020 mAh Battery",
        "camera": "13MP Rear Camera | 5MP Front Camera",
        "display": "13.84 cm (5.45 inch) Display",
        "processor": "Mediatek MT6739 Cortex A53 Processor",
        "ratings": "10,681",
        "resource": "2 GB RAM | 16 GB ROM | Expandable Upto 256 GB",
        "reviews": "851",
        "stars": "4.2",
        "title": "Honor 7S (Black, 16 GB)",
        "warranty": "12 Months on Handset, 6 Months on Battery, 6 Months on Charger & 3 Months on Data Cable"
    },
"""
class Product(models.Model):
	title = models.CharField(max_length=200)
	stars =  models.CharField(max_length=3)
	price =  models.CharField(max_length=20)
	active =  models.BooleanField(default=True)
	ratings = models.CharField(max_length=20)
	reviews = models.CharField(max_length=20)
	resource = models.TextField(max_length=200)
	display = models.CharField(max_length=200)
	camera = models.CharField(max_length=200)
	battery = models.CharField(max_length=200)
	processor = models.CharField(max_length=200)
	image = models.TextField(max_length=1000,default='https://cdn0.iconfinder.com/data/icons/entypo/98/mobile4-256.png')
	warranty = models.TextField(max_length=200)

	def __str__(self):
		return self.title+" - "+str(self.camera)+" - "+str(self.id)