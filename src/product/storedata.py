"""
	"battery": "4000 mAh Li Polymer Battery",
    "camera": "12MP + 5MP | 20MP Front Camera",
    "display": "15.21 cm (5.99 inch) Full HD+ Display",
    "processor": "Qualcomm Snapdragon 636 Processor",
    "ratings": "6,96,654",
    "resource": "4 GB RAM | 64 GB ROM | Expandable Upto 128 GB",
    "reviews": "83,791",
    "stars": "4.5",
    "title": "Redmi Note 5 Pro (Gold, 64 GB)",
    "warranty": "Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories"
"""


from .models import Product
import json

with open('.\\product\\productdetails.json','r+') as f:
	lines=f.read()
	l=json.loads(lines)
# print(l)

for d in l:
	reviews=d['reviews']
	title=d['title']
	battery=d['battery']
	processor=d['processor']
	ratings=d['ratings']
	stars=d['stars']
	camera=d['camera']
	resource=d['resource']
	display=d['display']
	warranty=d['warranty']
	price=d['price']
	product=Product(reviews=reviews,display=display,title=title,battery=battery,processor=processor,ratings=ratings,stars=stars,camera=camera,resource=resource,warranty=warranty,price=price)
	product.save()

print(product)
	