from django.shortcuts import render
from django.http import Http404,HttpResponse
from .models import Product

from django.conf import settings



def index(request):
	context={}
	return render(request,'index.html',context)

# def product(request,id):
# 	context={}
	
# 	return render(request,'product.html',context)


def products(request):
	context={}
	products=Product.objects.filter(active=True)
	
	return render(request,'products.html',{'products':products})


