from django.shortcuts import render
from django.http import Http404,HttpResponse
from django.shortcuts import get_object_or_404, render,redirect
from django.utils import timezone
from django import forms
from register.models import User
import os
import json
from django.conf import settings
from django.http import JsonResponse
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_protect
from django.template import loader
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
import hashlib


def send_email(subject, message, html_message, sender, recipients):
    print("SENDING MAIL: ", [
        subject, message, html_message, sender, recipients
    ])
    send_mail(
        subject,
        message,
        sender,
        recipients,
        fail_silently=False,
        html_message = html_message
    )
    print("Email thread is exiting")




@api_view(["POST", "GET"])
@parser_classes((JSONParser,))
def login(request):
    if request.method == "GET":
        template = loader.get_template("login.html")
        context = {}
        return HttpResponse(template.render(context, request))
    elif request.method=="POST":
        print("request data",request.data)
        try: 
            d=request.data
            print("d=",d)
            email=d['email']
            print("email",email)

            password=d['password']


            email2 = ''.join(reversed(email))
            password=password+email2
            print("password", password)

            password= hashlib.sha1(password.encode('utf-8')).hexdigest()
            print("password",password)
            
            user = User.objects.get(email=email,password=password)
            print (user)
            user.save()
            print("user saved") 
            if user.active==False:
               return Response({"active":user.active,"status":200,"message":"already deactived"}) 
            else:    
                return Response({"active":user.active,"status": 200, "userId":user.id,"message": "successfully logged in " + user.fullname }, status=200)
        except Exception as e: 
            print("user",e)
            return Response({"status": 400, "message": "Email does not exist in our PMT account"}, status=400)


def index(request):
    template = loader.get_template("home.html")
    context = {}
    return HttpResponse(template.render(context, request))

def contact(request):
    template = loader.get_template("contact.html")
    context = {}
    return HttpResponse(template.render(context, request))

def logout(request):
    template = loader.get_template("logout.html")
    context = {}
    return HttpResponse(template.render(context, request))