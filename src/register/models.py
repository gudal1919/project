from django.db import models
import datetime


TIME_ZONE =  'Asia/Kolkata'
# Create your models here.
class User(models.Model):

	active = models.BooleanField(default=True)
	fullname = models.CharField(max_length=200)
	email = models.CharField(max_length =200, unique=True)
	mobile = models.IntegerField(default =True, unique=True)
	address = models.TextField(editable = False)
	dob = models.CharField(max_length = 100 , default = '')
	image = models.ImageField(default=None,editable = False)
	password = models.CharField(max_length =200, default ='')
	country =models.CharField(max_length =100,default = '')
	state =models.CharField(max_length = 100, default ='')
	city = models.CharField(max_length = 100 , default = '')
	gender=models.CharField(max_length=200, editable = False)
	api_key = models.TextField(editable = False)
	added_date = models.DateTimeField(auto_now_add=datetime.datetime.now())
	updated_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.fullname+" - "+str(self.email)+"-"+str(self.id)
		
