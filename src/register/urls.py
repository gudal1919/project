from django.urls import path
from . import views
from django.contrib.auth.models import User
from django.conf import settings
from django.conf.urls.static import static

app_name = 'register'


urlpatterns = [
    path('', views.register, name='register'),
    
    path('profile/<id>',views.profile,name='profile'),
    
]