# Generated by Django 2.1.2 on 2018-10-24 11:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0009_auto_20181024_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='image',
            field=models.ImageField(default=None, editable=False, upload_to=''),
        ),
    ]
